TAM_FIX <- function(Ancrage, DATA_FIX) {
  ANCHOR =
    Ancrage %>%
    select(item,
           slope,
           threshold) %>%
    mutate(B = slope * 1.7) %>%
    mutate(A =  threshold * B)

  require(TAM)
  items = DATA_FIX %>% select(identifier) %>% unlist(use.names = FALSE) %>% unique()
  PersonItem =
    DATA_FIX %>%
    select(identifier,
           correct,
           poids = Poids_corrige,
           taker = `code barre`) %>%
    spread(key = identifier, value = correct) %>% data.table()
  Poids = PersonItem %>% select(poids) %>% unlist(use.names = FALSE)
  Eleves = PersonItem %>% select(taker) %>% unlist(use.names = FALSE)

  FIX =
    data.table(item = items) %>%
    left_join(y = ANCHOR, by = "item") %>%
    mutate(RANK = row_number()) %>%
    filter(!is.na(A)&!is.na(B))

  XSI_FIX = FIX %>% select(RANK, A) %>% as.matrix()
  B_FIX = FIX %>% mutate(Cat = 0, Dim = 1) %>%
    select(RANK, Cat, Dim, B) %>% as.matrix()

  TAM_FIX =
    TAM =
    tam.mml.2pl(
      resp = PersonItem %>% select(items),
      irtmodel = "2PL",
      pweights = Poids,
      pid = Eleves,
      xsi.fixed = XSI_FIX,
      B.fixed = B_FIX,
      control = list(progress = F,
                     increment.factor = 1.1)
    )
  return(TAM_FIX)
}
