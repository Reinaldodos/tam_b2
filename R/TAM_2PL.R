TAM_2PL = function(table,
                   identifier,
                   correct,
                   poids,
                   taker,
                   Groupe) {
  p_load(TAM)
  items = table %>% select_(identifier) %>% unlist(use.names = FALSE) %>% unique()
  PersonItem =
    table %>%
    select_(identifier,
            correct,
            poids,
            taker,
            Groupe) %>%
    rename_("identifier" = identifier,
            "correct" = correct) %>%
    spread(key = identifier, value = correct) %>% data.table()
  Poids = PersonItem %>% select_(poids) %>% unlist(use.names = FALSE)
  Eleves = PersonItem %>% select_(taker) %>% unlist(use.names = FALSE)
  Groupes = PersonItem %>% select_(Groupe) %>% unlist(use.names = FALSE)
  
  TAM =
    tam.mml.2pl(
      resp = PersonItem %>% select(items),
      irtmodel = "2PL",
      pweights = Poids,
      group = Groupes,
      pid = Eleves,
      control = list(progress = FALSE,
                     increment.factor = 1.1)
    )
  TAM %>% print()
  return(TAM)
}